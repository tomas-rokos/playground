library playground;

import "dart:html";

class Control {
	int _jump;
	Control(String txt,this._jump) {
		DivElement elm = new DivElement();
		document.body.append(elm);
		elm.text = txt;
		elm.style.position = "absolute";
		elm.style.top = "0px";
		elm.style.transition = "top 1s";
		elm.onClick.listen((MouseEvent e) {
			elm.style.top = "200px";
		});
	}
}