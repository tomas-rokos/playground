Dart setup
==========
This is detailed step by step process which takes (not counting download time) less then 5 minutes to complete

- Download Jetbrains WebStorm - https://www.jetbrains.com/webstorm/
- Download Dart SDK a Dartium - https://www.dartlang.org/downloads/archive/ -> Go for Stable Channel and 32bit version
- create folder “dart" somewhere on your computer
- unpack SDK into dart/dart-sdk
- unpack Dartium into dart/dartium
- run WebStorm
- go for trial and default settings
- inside create project go to Configure/Plugins, find Dart and hit OK
- WebStorm restarts
- inside create project, select Dart type, select path to SDK (dart/dart-sdk) and to Dartium (dart/dartium) inside path to project you specify nam of project, check “generate Sample Content” and choose “Uber Simple Web Application”
- you should see generated project and in console (embedded tool window) you see “Resolving dependencies”, it takes some time, so wait a bit, until you see “Process finished”
- inside project tree got to folder “web”, initiate context menu on index.html and choose “Run ‘index.html’” it should run Dartium and you should see a simple message that it’s running
- close Dartium, go again to context menu on index.html and choose “Debug ‘index.html’”, this will ends in bubble window inside WebStorm asking for support
- click on “Jetbrains IDE support”, it will bring you to Dartium into plugin page, go for “Add to Chrome”
- close Dartium
- inside project tree go to main.dart (doubleclick) and on row which starts with querySelector put breakpoint (e.g. by clicking on right side to row number), small filled circle should appear
- again go for context menu on index.html and go for Debug …
- during loading the page in Dartium, you will be switched to WebStorm and row with breakpoint is highlighted


CONGRATULATIONS !! You’re now ready to use awesome language Dart 

Fork this repo and try to enhance the Control class with one more constructor parameter for hover text and add the code to change to this text upon hover and change back when move out.

After opening this repo in WebStorm go to pubpec.yaml to enable Dart support and run Get dependencies.